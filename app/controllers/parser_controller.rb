# frozen_string_literal: true

# This is Controller for Parser
class ParserController < ApplicationController
  def index() end

  def import
    @lines = File.read(params[:file].path).lines
                 .map { |line| LineParseService.call(line.split(',')[0]) }
                 .compact.uniq.sort_by { |a| [a[0], a[1]] }
  end
end
