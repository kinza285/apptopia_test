# frozen_string_literal: true

# This is Service for Line Parser
class LineParseService < ApplicationService
  attr_reader :line
  MATCHES = [[%r{https:\/\/apptopia.com\/apps\/google_play\/(.+)}x,
              'Google Play'],
             [%r{https:\/\/apptopia.com\/apps\/itunes_connect\/(.+)}x,
              'Itunes Connect'],
             [%r{https:\/\/apptopia.com\/ios\/app\/(.+)\/intelligence}x,
              'Itunes Connect'],
             [%r{https:\/\/apptopia.com\/google-play\/app\/(.+)\/intelligence}x,
              'Google Play'],
             [%r{https:\/\/itunes.apple.com\/us\/app\/.+\/id(\d+)\?mt\=8}x,
              'Itunes Connect'],
             [%r{https:\/\/play.google.com\/store\/apps\/details\?id=(.+)&hl\=en}x,
              'Google Play']].freeze

  def initialize(line)
    @line = line
  end

  def call
    MATCHES.each do |match|
      if correct_match = line.match(match[0])
        return [match[1], correct_match.captures[0]]
      end
    end
    nil
  end
end
