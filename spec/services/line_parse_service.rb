# frozen_string_literal: true

require 'rails_helper'

describe LineParseService do
  it 'should parse Google Play from https://apptopia.com' do
    expect(LineParseService.call('https://apptopia.com/apps/google_play/com.netease.l10'))
      .to eq ['Google Play', 'com.netease.l10']
  end
  it 'should parse Itunes Connect from https://apptopia.com' do
    expect(LineParseService.call('https://apptopia.com/apps/itunes_connect/1253537199'))
      .to eq ['Itunes Connect', '1253537199']
  end
  it 'should parse Itunes Connect from https://apptopia.com/ios/app' do
    expect(LineParseService.call('https://apptopia.com/ios/app/1448852425/intelligence'))
      .to eq ['Itunes Connect', '1448852425']
  end
  it 'should parse Google Play from https://apptopia.com/google-play/app' do
    expect(LineParseService.call('https://apptopia.com/google-play/app/com.facebook.orca/intelligence'))
      .to eq ['Google Play', 'com.facebook.orca']
  end
  it 'should parse Itunes Connect from https://itunes.apple.com/us/app/fortnite' do
    expect(LineParseService.call('https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8'))
      .to eq ['Itunes Connect', '1261357853']
  end
  it 'should parse Google Play from https://play.google.com/store/apps' do
    expect(LineParseService.call('https://play.google.com/store/apps/details?id=com.roblox.client&hl=en'))
      .to eq ['Google Play', 'com.roblox.client']
  end
  it 'should not parse abracadabra' do
    expect(LineParseService.call('abracadabra'))
      .to be_nil
  end

end
